var haslo = ['Bez pracy nie ma kołaczy', 'Darowanemu koniowi w zęby się nie zagląda', 'Fortuna kołem się toczy', 'Nie chwal dnia przed zachodem słońca', 'Lepszy wróbel w garści niż gołąb na dachu', 'Apetyt rośnie w miarę jedzenia', 'Co ma wisieć, nie utonie', 'Dzieci i ryby głosu nie mają', 'Grosz do grosza a będzie kokosza', 'Łaska pańska na pstrym koniu jeździ'];
var haslo = haslo[Math.floor(Math.random()*haslo.length)];
haslo = haslo.toUpperCase();

var dlugosc = haslo.length;
var ile_prob = 0;

var tak = new Audio("tak.wav");
var nie = new Audio("nie.wav");

var skitrane = "";

for (i=0;i<dlugosc;i++)
{
    if(haslo.charAt(i)==" ") skitrane = skitrane + " ";
    else skitrane = skitrane + "-";
}


function wypisz_haslo()
{
    document.getElementById("plansza").innerHTML = skitrane;
}

window.onload = start;

var litery = "AĄBCĆDEĘFGHIJKLŁMNŃOÓPQRSŚTUVWXYZŻŹ";

function start()
{
    var literki = "";

    for (i=0;i<35;i++)
    {
        var element = "lit" + i;
        literki = literki + '<div class="litera" onclick="sprawdz('+i+')" id="'+element+'">'+litery[i]+'</div>';
        if ((i+1) % 7 == 0) literki = literki + '<div style="clear:both;"></div>';
    }

    document.getElementById("alfabet").innerHTML = literki;

    wypisz_haslo();
}

String.prototype.ustawZnak = function(miejsce, znak)
{
    if (miejsce > this.length - 1) return this.toString();
    else return this.substr(0, miejsce) + znak + this.substr(miejsce + 1);
}


function sprawdz(nr)
{
	
    var trafiona = false;
	
	for(i=0; i<dlugosc; i++)
	{
		if (haslo.charAt(i) == litery[nr]) 
		{
			skitrane = skitrane.ustawZnak(i,litery[nr]);
			trafiona = true;
		}
    }
	
	if(trafiona == true)
	{
		tak.play();
		var element = "lit" + nr;
		document.getElementById(element).style.background = "#003300";
		document.getElementById(element).style.color = "#00C000";
		document.getElementById(element).style.border = "3px solid #00C000";
        document.getElementById(element).style.cursor = "default";
		
		wypisz_haslo();
	}
	else
	{
		nie.play();
		var element = "lit" + nr;
		document.getElementById(element).style.background = "#330000";
		document.getElementById(element).style.color = "#C00000";
		document.getElementById(element).style.border = "3px solid #C00000";
		document.getElementById(element).style.cursor = "default";	
		document.getElementById(element).setAttribute("onclick",";");		
		
		ile_prob++;
		var obraz = "img/s"+ ile_prob + ".jpg";
		document.getElementById("szubienica").innerHTML = '<img src="'+obraz+'" alt="" />';
	}
	
	if (haslo == skitrane)
	document.getElementById("alfabet").innerHTML  = "Tak jest! Podano prawidłowe hasło: "+haslo+'<br /><br /><span class="reset_w" onclick="location.reload()">JESZCZE RAZ?</span>';
	
	if (ile_prob>=9)
    document.getElementById("alfabet").innerHTML  = "Przegrana! Prawidłowe hasło: "+haslo+'<br /><br /><span class="reset_l" onclick="location.reload()">JESZCZE RAZ?</span>';
}
