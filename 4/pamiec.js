let karty = ['ciri.png', 'geralt.png', 'jaskier.png', 'jaskier.png', 'iorweth.png', 'triss.png', 'geralt.png', 'yen.png', 'ciri.png', 'triss.png', 'yen.png', 'iorweth.png'];
var iloscKart = karty.length;
var listaKart = $('.plansza div');

tasowanie = (array) => array.sort(() => Math.random() - 0.5);
tasowanie(karty);

for (i=0;i<iloscKart;i++)
{
    document.getElementById('k' + i);
}

for (let i=0;i<iloscKart;i++)
{
    listaKart[i].addEventListener("click", function() {odslonKarte(i);});

}

var jednaWidoczna  = false;
var iloscRund = 0;
var widoczna;
var blokada = false;
var pozostalo = 6;

function odslonKarte(nr)
{
    var wartoscOpacity = $('#k' + nr).css('opacity');

    if (wartoscOpacity != 0 && blokada == false && nr != widoczna)
    {  
        blokada = true;

        var obraz = "url(img/" + karty[nr] + ")";
        $('#k' + nr).css('background-image', obraz);
        $('#k' + nr).addClass('kartaA');
        $('#k' + nr).removeClass('karta');

        if (jednaWidoczna == false)
        {
            jednaWidoczna = true;
            widoczna = nr;
            blokada = false;
        }
        else
        {
            if(karty[widoczna] == karty[nr])
            {
                setTimeout(function () {ukryj(nr, widoczna)}, 750);
                pozostalo--;
                if (pozostalo == 0)
                {
                    $('.plansza').html('<h1>Wygrałeś! <br>Udało się w ' + iloscRund + ' rund(y)</h1> <br><span class="przeladuj" onClick="location.reload()">Zagraj jeszcze raz!</span>');
                }
            }
            else
            {
                setTimeout(function () {przywroc(nr, widoczna)}, 1000);
            }

            iloscRund++;
            $('.wynik').html('Runda: ' + iloscRund);
            jednaWidoczna = false;
        }
    }
}

function ukryj(karta1, karta2)
{
    $('#k' + karta1).css('opacity', 0);
    $('#k' + karta2).css('opacity', 0);

    blokada = false;
}

function przywroc(karta1, karta2)
{
    $('#k' + karta1).css('background-image', 'url(img/karta.png)');
    $('#k' + karta1).addClass('karta');
    $('#k' + karta1).removeClass('kartaA');

    $('#k' + karta2).css('background-image', 'url(img/karta.png)');
    $('#k' + karta2).addClass('karta');
    $('#k' + karta2).removeClass('kartaA');

    blokada = false;
}